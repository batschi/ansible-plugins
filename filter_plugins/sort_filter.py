#!/usr/bin/env python
class FilterModule(object):
    def filters(self):
        return {
            'natural_sort': self.natural_sort
        }

    def natural_sort(self, p_list):
        p_list.sort(key=natural_keys)
        return p_list


def cast_to_type(item):
    return int(item) if item.isdigit() else item


def natural_keys(text):
    import re
    keys = re.split(r'(\d+)', text)
    keys_sorted = []
    for key in keys:
        keys_sorted.append(cast_to_type(key))
    return keys_sorted
